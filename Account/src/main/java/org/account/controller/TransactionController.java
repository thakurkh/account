package org.account.controller;

import org.account.entity.*;
import org.account.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "transaction")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping(path = "debit")
    ResponseEntity<TransactionResponse> debit(@Valid @RequestBody TransactionRequestEntity debitRequest) throws CustomException {
        Long statementId=transactionService.debit(debitRequest);

        return ResponseEntity.ok(new TransactionResponse("Debit done successfully",statementId));
    }

    @PostMapping(path = "credit")
    ResponseEntity<TransactionResponse> credit(@Valid @RequestBody TransactionRequestEntity creditRequest) throws CustomException {
        Long statementId=transactionService.credit(creditRequest);

        return ResponseEntity.ok(new TransactionResponse("Credit done successfully",statementId));
    }

    @GetMapping(path = "generateStatement")
    ResponseEntity<StatementHistoryResponse> getStatementHistory(@RequestParam String acc_no) throws CustomException
    {
        Long accNumber;
        try{
            accNumber=Long.parseLong(acc_no);
        }
        catch (NumberFormatException ex)
        {
            throw new CustomException("Please provide a valid numeric account number.");
        }
        StatementHistoryResponse statementHistoryResponse=new StatementHistoryResponse();
        statementHistoryResponse.setStatements(transactionService.generateStatementHistory(accNumber));
        return  ResponseEntity.ok(statementHistoryResponse);
    }

}
