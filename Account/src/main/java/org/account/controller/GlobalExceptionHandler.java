package org.account.controller;

import org.account.entity.CustomException;
import org.account.entity.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<Message> handleInValidRequest(MethodArgumentNotValidException validationException)
    {
        String errorMessage="Unable to process JSON. "+validationException.getBindingResult().getAllErrors().get(0).getDefaultMessage();

        return ResponseEntity.badRequest().body(new Message(errorMessage));
    }

    @ExceptionHandler
    public ResponseEntity<Message> handleCustomException(CustomException customException)
    {
        return ResponseEntity.ok(new Message(customException.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity<Message> handleJsonParserException(HttpMessageNotReadableException ex)
    {
        return ResponseEntity.ok(new Message("Unable to process Json"));
    }

}
