package org.account.controller;


import org.account.entity.*;
import org.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/account")
public class AccountController {

    @Autowired
    private AccountService accountService;


    @GetMapping(path = "/health")
    ResponseEntity<String> getStatus()
    {
        return ResponseEntity.ok("Up and Running");
    }

    @PostMapping
    ResponseEntity<CreateAccResponse> createNewAccount(@Valid @RequestBody AccountEntity accountEntity) throws CustomException {
        accountEntity=accountService.createAccount(accountEntity);
        return ResponseEntity.ok(new CreateAccResponse(accountEntity.getAcc_no(),"Account successfully created"));
    }

    @GetMapping(path = "/{accNumber}")
    ResponseEntity<FetchAccResponse> getAccount(@PathVariable String accNumber) throws CustomException
    {       Long acc_no;
            try{
                acc_no=Long.parseLong(accNumber);
            }
            catch (NumberFormatException ex)
            {
                throw new CustomException("Please provide a valid numeric account number.");
            }
            return ResponseEntity.status(HttpStatus.OK).body(new FetchAccResponse("Account fetched successfully.",accountService.getAccount(acc_no)));
    }

}
