package org.account.service;

import org.account.entity.*;
import org.account.repository.AccountRepository;
import org.account.repository.StatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    StatementRepository statementRepository;

    @Override
    public Long debit(TransactionRequestEntity debitRequest) throws CustomException {

        AccountEntity accountEntity=accountService.getAccount(debitRequest.getAcc_no());
        if(accountEntity.getAvailable_limit() < debitRequest.getAmount())
            throw new CustomException("Insufficient available balance");

        accountEntity.setAvailable_limit(accountEntity.getAvailable_limit()-debitRequest.getAmount());
        accountRepository.save(accountEntity);

        Statement statement=new Statement();
        statement.setAcc(debitRequest.getAcc_no());
        statement.setAmount(debitRequest.getAmount());
        statement.setStatus(Constants.TransactionStatus.SUCCESSFUL);
        statement.setType(Constants.TransactionType.DEBIT);

        statement=statementRepository.save(statement);

        return statement.getStatement_id();

    }

    @Override
    public Long credit(TransactionRequestEntity creditRequest) throws CustomException{

        AccountEntity accountEntity=accountService.getAccount(creditRequest.getAcc_no());
        if(accountEntity.getAvailable_limit() + creditRequest.getAmount() > accountEntity.getAuthorised_limit())
            throw new CustomException("Unable to do transaction. Authorised limit will be breached");

        accountEntity.setAvailable_limit(accountEntity.getAvailable_limit()+creditRequest.getAmount());
        accountRepository.save(accountEntity);

        Statement statement=new Statement();
        statement.setAcc(creditRequest.getAcc_no());
        statement.setAmount(creditRequest.getAmount());
        statement.setStatus(Constants.TransactionStatus.SUCCESSFUL);
        statement.setType(Constants.TransactionType.CREDIT);

        statement=statementRepository.save(statement);

        return statement.getStatement_id();

    }

    @Override
    public List<Statement> generateStatementHistory(Long accountNumber) throws CustomException {
        AccountEntity accountEntity=accountService.getAccount(accountNumber);
        return statementRepository.findByAcc(accountNumber);
    }
}
