package org.account.service;

import org.account.entity.AccountEntity;
import org.account.entity.CustomException;

public interface AccountService {
    AccountEntity createAccount(AccountEntity accountEntity) throws CustomException;
    AccountEntity getAccount(Long accountNumber) throws CustomException;

}
