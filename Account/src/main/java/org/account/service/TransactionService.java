package org.account.service;


import org.account.entity.CustomException;
import org.account.entity.Statement;
import org.account.entity.TransactionRequestEntity;
import org.springframework.stereotype.Service;

import java.util.List;


public interface TransactionService {

    Long debit(TransactionRequestEntity debitRequest)throws CustomException;
    Long credit(TransactionRequestEntity creditRequest) throws CustomException;
    List<Statement> generateStatementHistory (Long accountNumber) throws CustomException;

}
