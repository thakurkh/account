package org.account.service;

import org.account.entity.AccountEntity;
import org.account.entity.CustomException;
import org.account.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountRepository accountRepository;


    @Override
    public AccountEntity createAccount(AccountEntity accountEntity) throws CustomException
    {
        Optional<AccountEntity> optionalAccountEntity= accountRepository.findByEmail(accountEntity.getEmail());

        if(optionalAccountEntity.isPresent())
            throw new CustomException("Account with same email already exists.");

        accountRepository.save(accountEntity);
        return  accountEntity;
    }

    @Override
    public AccountEntity getAccount(Long accountNumber) throws CustomException {
        Optional<AccountEntity> accountEntityOptional=accountRepository.findById(accountNumber);
        if(accountEntityOptional.isPresent())
            return accountEntityOptional.get();
        throw new CustomException("Account does not exist");
    }

}
