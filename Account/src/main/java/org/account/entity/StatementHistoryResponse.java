package org.account.entity;

import lombok.Data;

import java.util.List;

@Data
public class StatementHistoryResponse {
    List<Statement> statements;
}
