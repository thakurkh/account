package org.account.entity;

import lombok.Data;

@Data
public class CreateAccResponse extends Message{
    private Long acc_no;

    public CreateAccResponse(Long acc_no, String message) {
        this.acc_no = acc_no;
        this.message=message;
    }
}
