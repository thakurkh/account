package org.account.entity;

public class CustomException extends Exception {
    public CustomException(String s) {
        super(s);
    }
}
