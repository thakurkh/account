package org.account.entity;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TransactionRequestEntity {

    @NotNull(message = "Please Provide a valid account number.")
    @Min(value = 1l,message = "Please Provide a valid account number greater than 0.")
    private Long acc_no;
    @NotNull(message = "Please Provide a valid amount greater than 0.")
    @Min(value = 1l,message = "Please Provide a valid amount greater than 0.")
    private Long amount;
    @NotEmpty(message = "Please provide a description.")
    private String description;
}
