package org.account.entity;

import lombok.Data;

@Data
public class FetchAccResponse extends Message {
    AccountEntity acc;

    public FetchAccResponse(String message, AccountEntity acc) {
        super(message);
        this.acc = acc;
    }

    public FetchAccResponse() {

    }
}
