package org.account.entity;



import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
@Table(name = "accounts",indexes = {@Index(name = "email_index",  columnList="email", unique = true)})
@Data
public class AccountEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long acc_no;

    @NotEmpty(message = "Please Provide Customer Name")
    private String customer_name;

    @Email(message = "Please provide a valid Email")
    private String email;

    @NotNull(message = "Please provide a valid Limit")
    @Min(value = 1,message = "Please provide limit greatter than 1")
    private Long authorised_limit;
    private Long available_limit;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created_date;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updated_date;


    @PrePersist
    protected void onCreate() {
        available_limit=0l;
        updated_date = created_date = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated_date = new Date();
    }


}
