package org.account.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "transactionHistory",indexes = {@Index(name = "acc_index",  columnList="acc")})
@Data

public class Statement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long statement_id;
    @NotNull
    private Long acc;
    @NotNull
    private Long amount;
    @NotNull
    private Constants.TransactionType type;
    @NotNull
    private Constants.TransactionStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created_date;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updated_date;


    @PrePersist
    protected void onCreate() {
        updated_date = created_date = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated_date = new Date();
    }
}
