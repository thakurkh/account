package org.account.entity;

import lombok.Data;

import javax.persistence.Transient;

@Data
public class Message {
    String message;

    public Message(String message) {
        this.message = message;
    }

    public Message() {

    }
}
