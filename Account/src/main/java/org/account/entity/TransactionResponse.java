package org.account.entity;

import lombok.Data;

@Data
public class TransactionResponse extends Message {

    private Long debit_id;

    public TransactionResponse(String message, Long debit_id) {
        super(message);
        this.debit_id = debit_id;
    }

    public TransactionResponse() {

    }
}
